# ESP8266 and Nova SDS011/SDS018

Sample code for reading out the Nova (诺方) SDS011 and SDS018 particulate matter
sensor (PM2.5 and PM10) from the ESP8266.
