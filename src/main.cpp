#include <Arduino.h>

// Set to 1 to enable software serial using two GPIO pins.
// Set to 0 to use a hardware UART (Serial, Serial1, etc.) instead.
#define USE_SOFTWARE_SERIAL     1

// Set to 1 when using a hardware UART that's shared between sensor and debug output.
// If set to 1 DEBUG_SERIAL and SENSOR_SERIAL can (and must) use the same hardware UART port and
// the implementation will use HardwareSerial::swap() to switch between the main and secondary set
// of pins.
// Set to 0 when using two independent hardware UART ports.
// If set to 1 DEBUG_SERIAL and SENSOR_SERIAL must use different HardwareSerial instances.
#define SHARED_SENSOR_SERIAL    0

#define DEBUG_SERIAL            Serial
#define SENSOR_SERIAL           Serial

#if USE_SOFTWARE_SERIAL
#include <SoftwareSerial.h>
#endif

namespace
{

    template<typename FilterPolicy>
    class FilteredSerial
        : public Stream
        , private FilterPolicy
    {
        HardwareSerial& backend_;

        using FilterPolicy::pre;
        using FilterPolicy::post;

    public:
        FilteredSerial(HardwareSerial& backend)
            : FilterPolicy{ backend }
            , backend_(backend) {}

        // Stream methods
        int     available()         override { pre(); const auto res = backend_.available(); post(); return res; }
        void    flush()             override { pre();                  backend_.flush();     post();             }
        int     peek()              override { pre(); const auto res = backend_.peek();      post(); return res; }
        int     read()              override { pre(); const auto res = backend_.read();      post(); return res; }
        size_t  write(uint8_t byte) override { pre(); const auto res = backend_.write(byte); post(); return res; }

        // HardwareSerial methods (the most important ones anyway)
        void    begin(unsigned long speed)                      { return backend_.begin(speed); }
        void    begin(unsigned long speed, SerialConfig config) { return backend_.begin(speed, config); }
        void    end()                                           { return backend_.end(); };
        void    swap()                                          { return backend_.swap(); }

        int     availableForWrite() { pre(); const auto res = backend_.availableForWrite(); post(); return res; }
    };

    class SwapFilterPolicy
    {
    private:
        HardwareSerial& backend_;

    protected:
        void pre() { backend_.swap(); }
        void post() { backend_.flush(); backend_.swap(); }

    public:
        SwapFilterPolicy(HardwareSerial& backend) : backend_(backend) {}
    };


#if USE_SOFTWARE_SERIAL
    auto  SSerial = SoftwareSerial{ D5, D6 };
    auto& DSerial = DEBUG_SERIAL;
#elif SHARED_SENSOR_SERIAL
    // If sensor and debug UART are shared we make the sensor UART the "master" and the debug UART
    // the "slave". The slave uses swap() around accessors.
    auto& SSerial = SENSOR_SERIAL;
    auto  DSerial = FilteredSerial<SwapFilterPolicy>{ DEBUG_SERIAL };
#else
    auto  SSerial = SENSOR_SERIAL;
    auto& DSerial = DEBUG_SERIAL;
#endif


    enum class Result
    {
        Success = 0,
        CrcError,
    };

    // SDS011 message format:
    // 0    Message header  AA
    // 1    Commander No.   C0
    // 2    DATA 0          PM2.5 Low byte
    // 3    DATA 1          PM2.5 High byte
    // 4    DATA 2          PM10 Low byte
    // 5    DATA 3          PM10 High byte
    // 6    DATA 4          ID byte 1
    // 7    DATA 5          ID byte 2
    // 8    Check-sum       Check-sum
    // 9    Message tail    AB
    Result
    ProcessMessage(byte buffer[9], float& pm25, float& pm10)
    {
        const auto expectedChecksum = [] (const byte *buf) -> byte {
            auto sum = 0;
            for(auto i = size_t{ 2 }; i <= 7; i++)
                sum += buf[i];
            return sum & 0xFF;
        };
        const auto checksum = buffer[8];
        if(checksum != expectedChecksum(buffer))
            return Result::CrcError;

        pm25 = (buffer[3] << 8) + (buffer[2] & 0xFF) / 10.0;
        pm10 = (buffer[5] << 8) + (buffer[4] & 0xFF) / 10.0;

        return Result::Success;
    }

}


void setup()
{
    SSerial.begin(9600);
    DSerial.begin(9600);
#if SHARED_SENSOR_SERIAL
    DSerial.swap();      // UART2 by default, switch to UART0 when printing debug messages
#endif
}


void loop()
{
    // The UART timeout is 1000 ms by default. The code should work fine with any timeout as long
    // as the buffer doesn't overflow.
    //SSerial.setTimeout(1000);

    byte buffer[9] = {};
    const auto bytesRead = SSerial.readBytesUntil(0xAB, buffer, sizeof(buffer) + 1);
    if(bytesRead == sizeof(buffer))
    {
        auto pm25 = float{}, pm10 = float{};
        const auto result = ProcessMessage(buffer, pm25, pm10);
        if(result == Result::Success)
        {
            DSerial.print("PM2.5: ");
            DSerial.print(pm25, 1);
            DSerial.print(", PM10: ");
            DSerial.print(pm10, 1);
            DSerial.println();
        }
        else if(result == Result::CrcError)
        {
            DSerial.println("ERROR: CRC mismatch.");
        }
    }
}
